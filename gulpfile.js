'use strict'

const gulp = require('gulp')
const uglify = require('gulp-uglify-es').default
const concat = require('gulp-concat')
const csso = require('gulp-csso')
const pump = require('pump')
const babel = require('gulp-babel')

gulp.task('css', (callback) => {
  pump([
    gulp.src([
      './assets/css/**/*.css',
      './node_modules/materialize-css/dist/css/materialize.min.css'
    ]),
    concat('styles.min.css'),
    csso(),
    gulp.dest('./public')
  ], callback)
})

gulp.task('html', (callback) => {
  pump([
    gulp.src('./views/**'),
    gulp.dest('./public/views')
  ], callback)
})

gulp.task('fonts', (callback) => {
  pump([
    gulp.src('./node_modules/materialize-css/dist/fonts/roboto/*'),
    gulp.dest('./public/fonts/roboto')
  ], callback)
})

gulp.task('images', (callback) => {
  pump([
    gulp.src('./assets/images/**/*.*'),
    gulp.dest('./public/images')
  ], callback)
})

gulp.task('extLibs', (callback) => {
  pump([
    gulp.src([
      './assets/extlibs/jquery-*.min.js',
      './node_modules/materialize-css/dist/js/materialize.min.js',
      './assets/extlibs/lodash.min.js',
      './assets/extlibs/angular.min.js',
      './assets/extlibs/angular-animate.min.js',
      './assets/extlibs/angular-aria.min.js',
      './assets/extlibs/angular-messages.min.js',
      './assets/extlibs/angular-route.min.js',
      './assets/extlibs/angular-resource.min.js',
      './angular/rappiApp.js'
    ]),
    babel({presets: ['es2015']}),
    uglify(),
    concat('libs.min.js'),
    gulp.dest('./public')
  ], callback)
})

gulp.task('templates', (callback) => {
  pump([
    gulp.src([
      './angular/components/**/**.html']),
    gulp.dest('./public/templates')
  ], callback)
})

gulp.task('productsModule', (callback) => {
  pump([
    gulp.src([
      './angular/components/products/products.module.js',
      './angular/components/products/products.component.js']),
    uglify(),
    concat('productsModule.min.js'),
    gulp.dest('./public/views/components')
  ], callback)
})

gulp.task('cartModule', (callback) => {
  pump([
    gulp.src([
      './angular/components/cart/cart.module.js',
      './angular/components/cart/cart.component.js']),
    uglify(),
    concat('cartModule.min.js'),
    gulp.dest('./public/views/components')
  ], callback)
})

gulp.task('modules', ['productsModule', 'cartModule'])
gulp.task('default', ['css', 'html', 'fonts', 'images', 'extLibs', 'modules', 'templates'])
gulp.task('watch', () => {
  gulp.watch('./views/**', ['html'])
  gulp.watch('./angular/**/*.js', ['modules'])
  gulp.watch('./angular/**/*.html', ['templates'])
  gulp.watch('./angular/rappiApp.js', ['extLibs'])
  gulp.watch(['./assets/css/**/*.css', './assets/css/**/*.scss'], ['css'])
})
