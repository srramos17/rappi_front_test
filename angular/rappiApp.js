'use strict'

angular.module('rappiApp', [
  'ngRoute',
  'ngAnimate',
  'ngMessages',
  'ngResource',
  'ModuleProducts',
  'ModuleCart'
])
.config(['$locationProvider', '$routeProvider',
  ($locationProvider, $routeProvider) => {
    $routeProvider
    .when('/', {
      template: '<products></products>'
    })
    .otherwise({
      redirectTo: '/'
    })

    $locationProvider.html5Mode(true)
  }])
