'use strict'

angular.module('ModuleProducts')
  .component('products', {
    templateUrl: './templates/products/products.template.html',
    controller: ['$http', function ($http) {
      let ct = this
      ct.Products = []
      ct.Productsbkp = []
      ct.cart = []
      ct.Categories = []
      ct.orderProperty = 'vendidos'
      ct.reverse = true
      ct.Filters = {}
      ct.showOrder = false

      ct.Init = () => {
        $('select').material_select()
        $('.button-collapse').sideNav({draggable: true})
        $('.view-cart').sideNav({
          menuWidth: 350,
          edge: 'right',
          draggable: true,
          closeOnClick: true
        })

        getProducts()
      }

      ct.AgregarAlCarro = (product) => {
        let index = ct.cart.indexOf(product)
        if (index === -1) {
          product.quantity = 1
          ct.cart.push(product)
        } else {
          ct.cart[index].quantity += 1
        }
      }

      ct.SetFilters = () => {
        if (ct.Filters.greaterthan30) {
          ct.Products = ct.Products.filter(a => a.price >= 30000)
        } else if (ct.Filters.lessthan10) {
          ct.Products = ct.Products.filter(a => a.price <= 10000)
        } else if (ct.Filters.topseller) {
          ct.orderProperty = 'vendidos'
        } else if (ct.Filters.soldout) {
          ct.Products = ct.Products.filter(a => a.stock === 0)
        } else if (ct.Filters.available) {
          ct.Products = ct.Products.filter(a => a.stock > 0)
        } else {
          if (ct.Products.length !== ct.Productsbkp.length) {
            ct.Products = ct.Productsbkp
          }
        }
      }

      ct.sortBy = (prop) => {
        ct.reverse = (ct.orderProperty === prop) ? !ct.reverse : false
        ct.orderProperty = prop
      }

      ct.CleanFIlters = () => {
        ct.search = ''
        ct.Filters.greaterthan30 = false
        ct.Filters.lessthan10 = false
        ct.Filters.topseller = false
        ct.Filters.soldout = false
        ct.Filters.available = false
      }

      const getProducts = () => {
        $http.get('/products').then((res) => {
          ct.Products = res.data.products
          ct.Productsbkp = res.data.products
          ct.Products.map(i => ct.Categories.push(i.category))
          ct.Categories = Array.from(new Set(ct.Categories))
        })
      }
    }]
  })
