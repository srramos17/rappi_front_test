'use strict'

angular.module('ModuleProducts', [
  'ngRoute',
  'ngAnimate',
  'ngMessages',
  'ModuleCart'
])
