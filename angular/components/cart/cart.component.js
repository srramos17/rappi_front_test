'use strict'

angular.module('ModuleCart')
  .component('cart', {
    templateUrl: './templates/cart/cart.template.html',
    bindings: {
      items: '='
    },
    controller: [function () {
      let ct = this

      ct.DropCartItem = (product) => {
        let index = ct.items.indexOf(product)
        ct.items.splice(index, 1)
      }
    }]
  })
