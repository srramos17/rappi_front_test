# Rappi UI Developer Test

## Usage

1. Run 'npm install' command to install dependencies.
2. run 'gulp' to compile the sources.
3. Run 'npm start' command to initialize local server.
4. Navigate to http://localhost:3000

  ```sh
  $ npm install -d
  $ gulp
  $ npm start
  ```

## Lint

1. Run 'npm run lint' to execute linter validation. This will be verified the use of best practices in the code.

## About

1. Build with AngularJs 1.6, Node v 9.0 and materialize patterns.
2. Using ES6 standard.
3. Progressive Web App (PWA) support. Open it in chrome browser and pin to the desktop.
4. The release resources for production are in the public folder.
5. Enjoy It.